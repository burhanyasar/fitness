class FoodGroup < ActiveRecord::Base
  belongs_to :main_food_group
  attr_accessible :calories, :carbs, :fat, :name, :protein, :main_food_group_id, :main_food_group
end
