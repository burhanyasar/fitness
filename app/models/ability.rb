class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    can :read, :all
    cannot :read, FoodLog
    cannot :read, Log

    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :dietitian
      can :manage, DietList, :user_id => user.id
      can :manage, DietList, :user_id => user.roles.select {|r| r.name == 'dietitian_of' }.map(&:resource_id)
      can :manage, DietItem do |diet_item|
        diet_item.diet_list.user_id == user.id
      end
      can :manage, DietItem do |diet_item|
        user.has_role? :dietitian_of, User.find(diet_item.diet_list.user_id)
      end
      can :read, Log, :user_id => user.roles.select {|r| r.name == 'dietitian_of' }.map(&:resource_id)
    elsif user.id?
      can :add_log, Food
      can :search_by_name, Food
      can :manage, FoodLog, :user_id => user.id
      can :create, FoodLog
      can :manage, Log, :user_id => user.id
    end

    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
