class DietList < ActiveRecord::Base
  attr_accessible :name
  belongs_to :user
  has_many :diet_items, dependent: :destroy

  validates :user_id, :name, presence: true
  after_save :log

  def self.prepared
    find_all_by_user_id(User.first)
  end
  
  protected
    def json_for_log
      to_json(except: [:id, :created_at], include: { diet_items: {only: [:amount, :main_food_group_id]}})
    end

  private
    def log
      diet_list_log = user.diet_list_logs.find_or_initialize_by_created_on(Date.today)
      diet_list_log.diet_list = self.json_for_log
      diet_list_log.save
    end
end
