class Food < ActiveRecord::Base
  extend Enumerize
  extend ActiveModel::Naming
  include PgSearch

  enumerize :weight_type, in: { g: 1, ml: 2 }, default: :g
  pg_search_scope :search_by_name,
    :against => :name,
    :ignoring => :accents,
    :order_within_rank => "foods.name ASC",
    :using => { :tsearch => {
      :prefix => true,
      :dictionary => "turkish" } }

  belongs_to :food_group
  attr_accessible :name, :weight, :food_group_id, :weight_type
  validates :name, :food_group, presence: true
  validates :weight, numericality: { greater_than_or_equal: 0.01 }
  validates :weight_type, numericality: { greater_than: 0, less_than: 3 }
  before_save :downcase_name

  def self.search(query)
    return search_by_name(query) unless query.blank?
    order(:name)
  end

  def name
    UnicodeUtils.titlecase(read_attribute(:name)) if read_attribute(:name)
  end

  private
    def downcase_name
      self.name = UnicodeUtils.downcase(self.name)
    end
end
