class Log
  attr_reader :date, :diet_list_log, :food_logs

  def initialize(user, date=nil)
    date = date || Date.today
    if user.diet_list_logs.on(date).last
      @date = date.to_s
      @diet_list_log = user.diet_list_logs.on(date).last.diet_list
      @diet_list_items = @diet_list_log['diet_items']
      @diet_list_log['diet_items'] = @diet_list_items.map { |di| DietItem.new(di) }
      @diet_list_log = DietList.new(@diet_list_log, without_protection: :true)
      @food_logs = user.food_logs.on(date)
    else
      @errors = [I18n.t('errors.models.log.not_found')]
    end
  end

  def exists?
    @errors.nil?
  end
end
