class DietItem < ActiveRecord::Base
  belongs_to :diet_list
  belongs_to :main_food_group
  attr_accessible :amount, :main_food_group, :main_food_group_id
  after_save :update_parent

  validates :main_food_group_id, :diet_list_id, presence: true
  validates :amount, numericality: { greater_than: 0 }

  private
    def update_parent
      diet_list.save
    end
end
