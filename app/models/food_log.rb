class FoodLog < ActiveRecord::Base
  extend Enumerize
  extend ActiveModel::Naming
  
  enumerize :meal, in: { breakfast: 1, lunch: 2, dinner: 3 }
  belongs_to :user
  belongs_to :food
  attr_accessible :weight, :date, :food, :food_id, :meal

  validates :weight, :food_id, :meal, :date, presence: true
  validates :weight, numericality: { greater_than: 0 }
  validates :meal, numericality: { greater_than: 0, less_than: 4 }
  scope :on, lambda { |date| { conditions: ["date = ?", date]}}

  def serializable_hash(options = nil)
    options ||= {}
    super({ include: :food, except: :food_id }.merge(options))
  end
end
