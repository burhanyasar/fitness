class User < ActiveRecord::Base
  rolify
  before_save :ensure_authentication_token
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body

  has_many :diet_lists
  has_many :diet_list_logs
  has_many :food_logs

  after_create :create_diet_list

  def use_diet_list(list_to_copy)
    diet_list = diet_lists.first || diet_lists.build
    diet_list.update_attribute :name, list_to_copy.name
    diet_list.diet_items.destroy_all
    list_to_copy.diet_items.each do |diet_item|
      diet_list.diet_items.create amount: diet_item.amount, main_food_group_id: diet_item.main_food_group.id
    end
    diet_list
  end

  def logs
    Log.new(self)
  end

  def serializable_hash(options = nil)
    options ||= {}
    super({ methods: [:authentication_token, :logs],
      include: [diet_lists: {include: :diet_items}] }.merge(options))
  end

  private
    def create_diet_list
      diet_lists.create(name: 'Diyetim')
    end
end
