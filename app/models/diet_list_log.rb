class DietListLog < ActiveRecord::Base
  attr_accessible :date, :diet_list

  scope :on, lambda { |date| { conditions: ["created_on <= ?", date]}}

  def diet_list
    ActiveSupport::JSON.decode(read_attribute(:diet_list))
  end
end
