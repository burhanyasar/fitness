ActiveAdmin.register Food do
  active_admin_import validate: true, timestamps: true, back: :index

  index do
    column :id
    column :name
    column :weight
    column :food_group
    column :updated_at
    default_actions
  end
end
