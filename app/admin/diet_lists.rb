ActiveAdmin.register DietList do
  actions :all, except: [:create, :new]
  index do
    column :id
    column :name
    column :user
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
    end
    f.buttons
  end
end
