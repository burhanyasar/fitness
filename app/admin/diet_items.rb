ActiveAdmin.register DietItem do
  actions :all, except: [:create, :new]
  form do |f|
    f.inputs do
      f.input :food_group
      f.input :amount
    end
    f.buttons
  end
end
