class FoodLogsController < InheritedResources::Base
  load_and_authorize_resource

  private
    def begin_of_association_chain
      current_user
    end
end
