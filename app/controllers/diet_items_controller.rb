class DietItemsController < InheritedResources::Base
  load_and_authorize_resource
  belongs_to :diet_list

  def new
  	@collection = MainFoodGroup.all -
  	  @diet_list.diet_items.map{|di| di.main_food_group}
  	new!
  end
  def create
    create! {diet_list_diet_items_path(@diet_list)}
  end

  def update
    update! {diet_list_diet_items_path(@diet_list)}
  end

  def edit
  	@collection = MainFoodGroup.all -
  	  @diet_list.diet_items.map{|di| di.main_food_group} +
      [ @diet_item.main_food_group ]
    edit!
  end
end