class DietListsController < InheritedResources::Base
  load_and_authorize_resource

  def index
    @diet_lists = current_user.diet_lists
    index!
  end

  def show
    redirect_to diet_list_diet_items_path(@diet_list)
  end

  def show_prepared
    @diet_lists = DietList.prepared
  end

  def update
    update! { diet_list_diet_items_path(@diet_list) }
  end
end