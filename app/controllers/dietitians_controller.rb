class DietitiansController < ApplicationController
  include ::DietitiansHelper
  before_filter :authenticate_dietitian!

  def index
  end

  def users_create
    @user = User.create({ password: rand(36**9).to_s(36) }.merge(params[:user]))
    if @user.errors.empty?
      current_user.add_role :dietitian_of, @user
      render 'users_show'
    else
      render 'users_new'
    end
  end

  def users_new
    @user = User.new
  end

  def users_index
    @users = current_user.roles.select { |r| r.name == 'dietitian_of' }.map { |r| User.find(r.resource_id) }
  end

  def assign_diet_list
    user = User.find(params[:user_id])
    diet_list = DietList.find(params[:diet_list_id])
    diet_list = user.use_diet_list(diet_list)
    respond_to do |format|
      format.html { redirect_to user_logs_with_date_path(user, Date.today) }
      format.json { render json: diet_list }
    end
  end

  def lists_to_assign
    @user = User.find(params[:user_id])
    @diet_lists = current_user.diet_lists
    render @diet_lists
  end
end
