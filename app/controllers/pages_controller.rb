class PagesController < ApplicationController
  def home
    respond_to do |format|
      format.html do
      	redirect_to dietitians_path if current_user and current_user.has_role? :dietitian
      end
      format.json do
        render :json => current_user.to_json(include: {diet_lists: {include: :diet_items}})
      end
    end
  end
end
