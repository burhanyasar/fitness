require "application_responder"

class ApplicationController < ActionController::Base
  respond_to :html
  respond_to :json

  protect_from_forgery

  def authenticate_admin_user!
	  authenticate_user!
	  unless current_user.has_role? :admin
      respond_to do |format|
        flash[:alert] = t :only_admins, scope: :alert
        format.html { redirect_to root_path }
        format.json { render json: { error: flash[:alert] }, status: 403 }
      end
	  end
	end

	def current_admin_user
	  return nil if user_signed_in? and !current_user.has_role? :admin
	  current_user
	end

	def redirect_back_or(default = root_path, *options)
    tag_options = {}
    options.first.each { |k,v| tag_options[k] = v } unless options.empty?
    redirect_to (request.referer.present? ? :back : default), tag_options
  end

	rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      flash[:alert] = t :not_authorized, scope: :alert
      format.html { redirect_back_or root_path }
      format.json { render json: { error: flash[:alert] }, status: 403 }
    end
  end
end
