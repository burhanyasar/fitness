class SessionsController < ApplicationController
  before_filter :authenticate_user!

  def validate_token
    respond_with(params.fetch(:authentication_token, "") == current_user.authentication_token)
  end
end