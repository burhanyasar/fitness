class LogsController < ApplicationController
  load_and_authorize_resource

  def show
    if params[:user_id].blank?
      @user = current_user 
    else
      @user = User.find(params[:user_id])
    end
    @log = Log.new(@user, params[:date])
    if @log.exists?
      respond_with @log
    else
      respond_to do |format|
        format.html { render 'public/404.html', status: 404 }
        format.json { render json: @log, status: 404 }
      end
    end
  end
end
