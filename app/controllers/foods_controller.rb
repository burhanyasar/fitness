class FoodsController < InheritedResources::Base
  load_and_authorize_resource
  has_scope :page, default: 1

  def index
    @foods = Food.search(params[:search]).page(params[:page])
    index!
  end

  # TODO: Guess meal from time
  def add_log
    @food_log = current_user.food_logs.build(food_id: params[:id], weight: 1.0,
                                             date: Date.today, meal: :breakfast)
  end
end