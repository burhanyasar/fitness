module DietitiansHelper

	def authenticate_dietitian!
	  authenticate_user!
	  unless (current_user.has_role?(:dietitian) or current_user.has_role?(:admin))
      respond_to do |format|
        flash[:alert] = t :only_dietitians, scope: :alert
        format.html { redirect_to root_path }
        format.json { render json: { error: flash[:alert] }, status: 403 }
      end
	  end
	end
end