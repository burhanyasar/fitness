# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

MainFoodGroup.delete_all
sut   = MainFoodGroup.create(name: 'Süt')
et    = MainFoodGroup.create(name: 'Et')
kuru  = MainFoodGroup.create(name: 'Kuru baklagil')
sebze = MainFoodGroup.create(name: 'Sebze')
meyve = MainFoodGroup.create(name: 'Meyve')
ekmek = MainFoodGroup.create(name: 'Ekmek-Tahıl')
yag   = MainFoodGroup.create(name: 'Yağ')
tatli = MainFoodGroup.create(name: 'Tatlı')

FoodGroup.delete_all
FoodGroup.create([
  { name: 'Süt',           calories: 114, carbs: 9,  fat: 6, protein: 6, main_food_group: sut },
  { name: 'Et',            calories: 64,  carbs: 0,  fat: 5, protein: 6, main_food_group: et },
  { name: 'Kuru baklagil', calories: 80,  carbs: 15, fat: 0, protein: 5, main_food_group: kuru },
  { name: 'SebzeA',        calories: 28,  carbs: 6,  fat: 0, protein: 1, main_food_group: sebze },
  { name: 'SebzeB',        calories: 36,  carbs: 7,  fat: 0, protein: 2, main_food_group: sebze },
  { name: 'Meyve',         calories: 48,  carbs: 12, fat: 0, protein: 0, main_food_group: meyve },
  { name: 'Ekmek-Tahıl',   calories: 68,  carbs: 15, fat: 0, protein: 2, main_food_group: ekmek },
  { name: 'Yağ',           calories: 45,  carbs: 0,  fat: 5, protein: 0, main_food_group: yag },
  { name: 'Tatlı',         calories: 20,  carbs: 5,  fat: 0, protein: 0, main_food_group: tatli }])

User.delete_all
admin  = User.create(email: 'admin@diyetisyen.herokuapp.com', password: rand(36**9).to_s(36))
burhan = User.create(email: 'burhanyasar@gmail.com',     password: 'fitol_pass')
ufuk   = User.create(email: 'ufukkpolat@gmail.com',      password: 'fitol_pass')
esra   = User.create(email: 'esrasonmezkula@gmail.com',  password: 'fitol_pass')
user   = User.create(email: 'user@mail.com',             password: '12345678')

burhan.add_role :admin
ufuk.add_role :admin
esra.add_role :admin

DietList.delete_all
admin.diet_lists.create(name: 'Diyet 1800-1').diet_items.create([
  { main_food_group: sut,   amount: 3},
  { main_food_group: et,    amount: 5},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 4},
  { main_food_group: yag,   amount: 4}])

admin.diet_lists.create(name: 'Diyet 1800-2').diet_items.create([
  { main_food_group: sut,   amount: 3},
  { main_food_group: et,    amount: 4},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 4},
  { main_food_group: yag,   amount: 5}])

admin.diet_lists.create(name: 'Diyet 1800-3').diet_items.create([
  { main_food_group: sut,   amount: 2},
  { main_food_group: et,    amount: 2},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 4},
  { main_food_group: yag,   amount: 6}])

admin.diet_lists.create(name: 'Diyet 1900-1').diet_items.create([
  { main_food_group: sut,   amount: 3},
  { main_food_group: et,    amount: 5},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 4},
  { main_food_group: yag,   amount: 5}])

admin.diet_lists.create(name: 'Diyet 2000-1').diet_items.create([
  { main_food_group: sut,   amount: 3},
  { main_food_group: et,    amount: 5},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 5},
  { main_food_group: yag,   amount: 5}])

admin.diet_lists.create(name: 'Diyet 2000-2').diet_items.create([
  { main_food_group: sut,   amount: 3},
  { main_food_group: et,    amount: 2},
  { main_food_group: sebze, amount: 2},
  { main_food_group: meyve, amount: 6},
  { main_food_group: yag,   amount: 8}])