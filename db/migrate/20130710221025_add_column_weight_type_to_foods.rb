class AddColumnWeightTypeToFoods < ActiveRecord::Migration
  def change
    add_column :foods, :weight_type, :integer, default: 1
  end
end
