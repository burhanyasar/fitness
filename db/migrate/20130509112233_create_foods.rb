class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :name
      t.float :weight
      t.references :food_group

      t.timestamps
    end
    add_index :foods, :food_group_id
    add_index :foods, :name, unique: true
  end
end
