class CreateDietLists < ActiveRecord::Migration
  def change
    create_table :diet_lists do |t|
      t.references :user
      t.string :name

      t.timestamps
    end
    add_index :diet_lists, :user_id
  end
end
