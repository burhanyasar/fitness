class CreateDietListLogs < ActiveRecord::Migration
  def change
    create_table :diet_list_logs do |t|
      t.text :diet_list
      t.date :created_on
      t.references :user
    end
    add_index :diet_list_logs, [:user_id, :created_on], :unique => true
  end
end
