class ChangeFoodGroupColumnToMainFoodGroupInDietItems < ActiveRecord::Migration
  def up
    rename_column :diet_items, :food_group_id, :main_food_group_id
    remove_index :diet_items, [:diet_list_id, :food_group_id]
    add_index :diet_items, [:diet_list_id, :main_food_group_id], :unique => true
  end

  def down
    rename_column :diet_items, :main_food_group_id, :food_group_id
    remove_index :diet_items, [:diet_list_id, :main_food_group_id]
    add_index :diet_items, [:diet_list_id, :food_group_id], :unique => true
  end
end
