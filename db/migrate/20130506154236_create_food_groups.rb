class CreateFoodGroups < ActiveRecord::Migration
  def change
    create_table :food_groups do |t|
      t.string :name
      t.float :calories
      t.float :carbs
      t.float :fat
      t.float :protein

      t.timestamps
    end
  end
end
