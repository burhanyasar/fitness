class CreateDietItems < ActiveRecord::Migration
  def change
    create_table :diet_items do |t|
      t.references :diet_list
      t.references :food_group
      t.integer :amount

      t.timestamps
    end
    add_index :diet_items, [:diet_list_id, :food_group_id], :unique => true
  end
end
