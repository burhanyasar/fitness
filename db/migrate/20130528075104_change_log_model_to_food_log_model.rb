class ChangeLogModelToFoodLogModel < ActiveRecord::Migration
  def up
    drop_table :logs
    create_table :food_logs do |t|
      t.references :user
      t.references :food
      t.integer :amount
      t.date :date
      t.integer :meal

      t.timestamps
    end
    add_index :food_logs, [ :user_id, :created_at ]
  end

  def down
    drop_table :food_logs
    create_table :logs do |t|
      t.references :user
      t.references :loggable, polymorphic: true
      t.integer :amount

      t.timestamps
    end
    add_index :logs, [ :user_id, :loggable_type, :loggable_id ]
    add_index :logs, [ :user_id, :created_at ]
  end
end
