class ChangeFoodLogColumnAmountToWeight < ActiveRecord::Migration
  def change
  	rename_column :food_logs, :amount, :weight
  end
end
