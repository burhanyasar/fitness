class AddMainFoodGroupReferenceToFoodGroups < ActiveRecord::Migration
  def change
    change_table :food_groups do |t|
      t.references :main_food_group
    end
  end

  def down
    remove_column :food_groups, :main_food_group_id
  end
end
