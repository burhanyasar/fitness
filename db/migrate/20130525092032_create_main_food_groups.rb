class CreateMainFoodGroups < ActiveRecord::Migration
  def change
    create_table :main_food_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
