class AddExtensionsToPg < ActiveRecord::Migration
  def self.up
    execute "CREATE EXTENSION unaccent"
  end

  def self.down
    execute "DROP EXTENSION unaccent"
  end
end
