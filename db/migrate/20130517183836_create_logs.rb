class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.references :user
      t.references :loggable, polymorphic: true
      t.integer :amount

      t.timestamps
    end
    add_index :logs, [ :user_id, :loggable_type, :loggable_id ]
    add_index :logs, [ :user_id, :created_at ]
  end
end
