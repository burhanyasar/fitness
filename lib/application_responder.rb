class ActionController::Responder
  #include Responders::FlashResponder
  #include Responders::HttpCacheResponder

  # Uncomment this responder if you want your resources to redirect to the collection
  # path (index action) instead of the resource path for POST/PUT/DELETE requests.
  # include Responders::CollectionResponder
  def json_resource_errors
    {:errors => resource.errors.full_messages}
  end
end
