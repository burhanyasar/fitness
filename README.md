# README #


To run the project

Install ruby version 1.9.3-p392, and also bundler if it's not already installed. 

    $rbenv install 1.9.3-p392
    $gem install bundler

Project uses postgresql also on the local development environment, for bundler to succeed you need to have postgresql installed or you can try updating the gem file to use sqlite.

For postgresql, you need to have user fitness or you can again adjust your database.yml

    $createuser -s --username=$USER fitness

or after connecting pgsql

    CREATE ROLE fitness WITH LOGIN CREATEDB

Then before running the project create the db, run the migrations and to add sample data and users seed the db

    $rake db:create
    $rake db:migrate
    $rake db:seed