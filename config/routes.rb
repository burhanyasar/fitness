Fitness::Application.routes.draw do
  root to: 'pages#home'

  ActiveAdmin.routes(self)

  devise_for :users

  get 'users/validate_token' => 'sessions#validate_token', as: :validate_token

  resources :diet_lists do
    resources :diet_items
  end

  resources :main_food_groups, only: [:index, :show]
  resources :food_groups, only: [:index, :show]
  resources :food_logs, except: [:new, :index]

  match 'foods/add_log/:id' => 'foods#add_log', via: [:get, :post], as: :add_food_log
  resources :foods, only: [:index, :show]

  # Custom controllers
  get 'logs' => 'logs#show'
  get 'logs/:date' => 'logs#show', as: :logs_with_date
  get 'pages/home'

  get 'dietitians' => 'dietitians#index', as: :dietitians
  get 'dietitians/users/new' => 'dietitians#users_new', as: :new_dietitian_user
  get 'dietitians/users' =>  'dietitians#users_index', as: :dietitian_user
  get 'dietitians/users/:user_id/logs/:date' => 'logs#show', as: :user_logs_with_date
  get 'dietitians/users/:user_id/lists_to_assign' => 'dietitians#lists_to_assign', as: :lists_to_assign
  get 'dietitians/users/:user_id/assign_diet_list/:diet_list_id' => 'dietitians#assign_diet_list', via: :get, as: :assign_diet_list
  post 'dietitians/users' => 'dietitians#users_create'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
