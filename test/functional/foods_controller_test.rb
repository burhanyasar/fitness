# encoding: utf-8
require 'test_helper'

class FoodsControllerTest < ActionController::TestCase
  setup do
    @user = users(:user_1)
    @food = foods(:food_1)
    sign_in @user
  end

  test "should get index" do
    get :index, format: :json
    assert_equal 6, ActiveSupport::JSON.decode(response.body).count
    assert_response :success
  end

  test "search food should work correctly with pg_search" do
    get :index, format: :json
    assert_equal ActiveSupport::JSON.decode(response.body).count, Food.all.count

    get :index, format: :json, search: "ç"
    result = ActiveSupport::JSON.decode(response.body)

    get :index, format: :json, search: "Ç"
    assert_equal result, ActiveSupport::JSON.decode(response.body)

    get :index, format: :json, search: "Süt"
    assert_equal 2, ActiveSupport::JSON.decode(response.body).count

    get :index, format: :json, search: "Sut"
    assert_equal 2, ActiveSupport::JSON.decode(response.body).count
  end

  test "should show food" do
    get :show, id: @food
    assert_response :success
  end

  test "should not get new" do
    assert_raise(ActionController::RoutingError) do
      get :new
    end
  end

  test "should not create food" do
    assert_raise(ActionController::RoutingError) do
      post :create, { name: 'test' }
    end
  end

  test "should not get edit" do
    assert_raise(ActionController::RoutingError) do
      get :edit, id: @food
    end
  end

  test "should not update food" do
    assert_raise(ActionController::RoutingError) do
      put :update, id: @food, food: { name: 'updated_name' }
    end
  end

  test "should not destroy food" do
    assert_raise(ActionController::RoutingError) do
      delete :destroy, id: @food
    end
  end
end
