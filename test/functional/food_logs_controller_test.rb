require 'test_helper'

class FoodLogsControllerTest < ActionController::TestCase
  setup do
    @user = users(:user_1)
    @food_log = food_logs(:food_log_1)
    sign_in @user
  end

  test "should not get index" do
    assert_raise(ActionController::RoutingError) do
      get :index
    end
  end

  test "should not get new" do
    assert_raise(ActionController::RoutingError) do
      get :new
    end
  end

  test "should create food_log" do
    food_1 = foods(:food_1)
    assert_difference('FoodLog.count') do
      post :create, food_log: { weight: @food_log.weight, food_id: food_1.id,
                                meal: :breakfast, date: Date.today }
    end

    assert_redirected_to food_log_path(assigns(:food_log))
  end

  test "should show food_log" do
    get :show, id: @food_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @food_log
    assert_response :success
  end

  test "should update food_log" do
    put :update, id: @food_log, food_log: { weight: @food_log.weight }
    assert_redirected_to food_log_path(assigns(:food_log))
  end

  test "should update meal with both numbers and strings" do
    p @food_log
    put :update, id: @food_log, food_log: { meal: 2 }
    assert_equal 2, @food_log.reload.meal.value
    
    # this doesn't work for now. stupid
    put :update, id: @food_log, food_log: { meal: :breakfast }
    assert_equal 'breakfast', @food_log.reload.meal
  end

  test "should destroy food_log" do
    assert_difference('FoodLog.count', -1) do
      delete :destroy, id: @food_log
    end

    assert_redirected_to food_logs_path
  end
end
