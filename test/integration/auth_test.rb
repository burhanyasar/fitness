require 'test_helper'

class AuthTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:user_1)
    @user_hash = { email: @user.email, password: 'password' }
    @new_user_hash = { email: 'new_user@mail.com', password: 'password' }
    @diet_list = diet_lists(:diet_list_1)
  end

  test "guest users should be redirected" do
    post diet_list_diet_items_path(@diet_list)
    assert_redirected_to root_path

    back = "/back"
    delete diet_list_diet_item_path(@diet_list, diet_items(:one_1)), {}, {'HTTP_REFERER' => back }
    assert_redirected_to back
  end

  test "users cannot enter into admin area" do
    login(@user)

    get_via_redirect admin_root_path
    assert_equal "This area is restricted to administrators only.", flash[:alert]
    assert_equal path, root_path
  end

  test "admins can access admin area" do
    @user.add_role :admin
    @user.save
    login(@user)
    get_via_redirect admin_root_path
    assert_equal path, admin_root_path
  end

  test "register via post" do
    post user_registration_path, { user: @new_user_hash, format: :json }
    assert_response :success
  end

  test "dont register user with existing email" do
    post user_registration_path, { user: @user_hash, format: :json }
    assert_response 422
  end

  test "login via post includes authentication_token" do
    post user_session_path, { user: @user_hash, format: :json }
    assert_equal ActiveSupport::JSON.decode(response.body)['authentication_token'],
      @user.reload.authentication_token
    assert_response :success
  end

  test "invalid login via post" do
    unknown = { email: 'unknown@mail.com' }
    post user_session_path, { user: unknown, format: :json }
    assert_response 401

    unknown[:password] = 'password'
    post user_session_path, { user: unknown, format: :json }
    assert_response 401
  end

  test "updating password after a couple of invalid trials" do
    ep = @user.encrypted_password

    post user_session_path, { user: @user_hash, format: :json }
    token = ActiveSupport::JSON.decode(response.body)['authentication_token']

    @user_hash[:password] = @user_hash[:password_confirmation] = 'parola'
    put user_registration_path, { user: @user_hash, authentication_token: token, format: :json }
    assert_response 422

    @user_hash[:current_password] = 'passwor'
    @user_hash[:password] = @user_hash[:password_confirmation] = 'parolauzun'
    put user_registration_path, { user: @user_hash, authentication_token: token, format: :json }
    assert_response 422
    assert_equal ep, @user.reload.encrypted_password

    @user_hash[:current_password] = 'password'
    put user_registration_path, { user: @user_hash, authentication_token: token, format: :json }
    assert_not_equal ep, @user.reload.encrypted_password
  end

  test "validating token" do
    # users cannot validate token unless authenticated
    get validate_token_path, { authentication_token: 'invalid_token', format: :json }
    assert_response 401

    @user.save!
    get validate_token_path, { authentication_token: @user.authentication_token, format: :json }
    assert ActiveSupport::JSON.decode(response.body)
    assert_response :success

    # after authentication response code is 200; however, response body is false
    get validate_token_path, { authentication_token: 'invalid_again', format: :json }
    refute ActiveSupport::JSON.decode(response.body)
    assert_response :success

    delete destroy_user_session_path
    # now we're logget out, we will get a 401, like we did at the beginning
    get validate_token_path, { authentication_token: 'invalid_again', format: :json }
    assert_response 401
  end
end
