require 'test_helper'

class DietListTest < ActiveSupport::TestCase
  setup do
    @diet_list = diet_lists(:diet_list_1)
  end

  test "responds to" do
    assert_respond_to @diet_list, :name
    assert_respond_to @diet_list, :user
    assert_respond_to @diet_list, :diet_items
  end

  test "number of diet_items" do
    assert_equal 7, @diet_list.diet_items.count
    assert_equal 4, diet_lists(:diet_list_2).diet_items.count
  end

  test "validation" do
    dl = DietList.new
    assert !dl.valid?

    dl.user_id = 1
    assert !dl.valid?

    dl.name = ""
    assert !dl.valid?

    dl.name = "Non-empty"
    assert dl.valid?
  end

  test "accessible attributes" do
    assert_raise(ActiveModel::MassAssignmentSecurity::Error) do
      DietList.new user_id: users(:user_1).id
    end
  end
end
