require 'test_helper'

class DietItemTest < ActiveSupport::TestCase
  setup do
    @diet_item = diet_items(:one_1)
  end

  test "responds to" do
    assert_respond_to @diet_item, :main_food_group
    assert_respond_to @diet_item, :diet_list
    assert_respond_to @diet_item, :amount
  end

  test "delete diet items on diet list destroy" do
    assert_raise(ActiveRecord::RecordNotFound) do
      diet_lists(:diet_list_1).destroy
      DietItem.find(@diet_item.id)
    end
  end
end
