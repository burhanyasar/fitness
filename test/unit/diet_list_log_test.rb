require 'test_helper'

class DietListLogTest < ActiveSupport::TestCase
  setup do
    @diet_list = diet_lists(:diet_list_1)
  end

  test "log must be created and updated" do
    @diet_list.save
    dl_log = @diet_list.user.diet_list_logs.first
    assert dl_log, "Diet list log not found for #{@diet_list}"
    old_diet_list = dl_log.diet_list

    @diet_list.name = 'Test'
    @diet_list.save
    assert_not_equal dl_log.reload.diet_list, old_diet_list
  end
end
