require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:user_1)
  end

  test "responds to" do
    assert_respond_to @user, :email
    assert_respond_to @user, :diet_lists
  end
end
