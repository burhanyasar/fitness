require 'test_helper'

class FoodGroupTest < ActiveSupport::TestCase
  setup do
    @food_group = food_groups(:ekmek)
  end

  test "responds to" do
    assert_respond_to @food_group, :name
    assert_respond_to @food_group, :calories
    assert_respond_to @food_group, :carbs
    assert_respond_to @food_group, :fat
    assert_respond_to @food_group, :protein
  end
end
