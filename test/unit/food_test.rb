require 'test_helper'

class FoodTest < ActiveSupport::TestCase
  setup do
    @food = foods(:food_1)
  end

  test "responds to" do
    assert_respond_to @food, :name
    assert_respond_to @food, :food_group
  end
end
